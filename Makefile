LIBS := -lzmq -lm
CC := gcc
CFLAGS := -g
PROGRAMS := worker broker client

.PHONY: clean all

all: $(PROGRAMS) wgo

wgo: worker.go
	go build -o $@ $<

$(PROGRAMS): %: %.c
	$(CC) $(CFLAGS) $(LIBS) -o $@ $<

clean:
	rm $(PROGRAMS) wgo
