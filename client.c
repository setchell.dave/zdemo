//  Connects REQ socket to tcp://localhost:5559

#include "zhelpers.h"

int main (int argc, char *argv[]) 
{
	void *context = zmq_ctx_new ();
	char msg_str[1000];
	int snd_msg[] = {
		100, 222, 459, 93231, 19231, 9812, 90, 101, 3554, 33333};
	int req_count=50;
	// atoi is bad and i should feel bad
	if (argc >= 3) { req_count = atoi(argv[2]); }

	//  Socket to talk to server
	void *requester = zmq_socket (context, ZMQ_REQ);
	zmq_connect (requester, "tcp://localhost:5559");

	int request_nbr;
	printf("Sending %d msgs\n", req_count);
	for (request_nbr = 0; request_nbr != req_count; request_nbr++) {
		size_t index = request_nbr % (sizeof(snd_msg) / sizeof(int));
		snprintf(msg_str, 1000, "%d", snd_msg[index]);
		s_send (requester, msg_str);
		char *string = s_recv (requester);
		printf ("Received reply %d with msg [%s]\n", request_nbr, string);
		free (string);
	}
	zmq_close (requester);
	zmq_ctx_destroy (context);
	return 0;
}
