module foo/zdemo

go 1.18

require (
	github.com/lib/pq v1.10.7
	github.com/pebbe/zmq4 v1.2.9
)
