trap "trap - TERM && kill -- -$$" INT TERM EXIT HUP

_reset_db () {
	psql -AXtq -c "TRUNCATE TABLE zdemo;" "$P"
	psql -AXtq -f initdb.sql "$P"
	psql -AXtq -f loaddb.sql "$P"
}

_run () {
	make
	./broker &
	for x in $(seq ${1:-3})
	do
		$C && ./worker "C_$x" &
		$GO && ./wgo "GO_$x" &
		$PY && python3 ./worker.py "PY_$x" &
	done
	PIDS=""
	for y in $(seq ${2:-3})
	do
		./client req_$y $msg_count &
		PIDS="$PIDS $!"
	done
	wait $PIDS
}

: ${P:="postgres://postgres@localhost:5432?sslmode=disable"}
while getopts m:hrpgac arg
do
    case "$arg" in
	r) _reset_db ;;
	p) PY=true ;;
	g) GO=true ;;
	c) C=true ;;
	a) C=true; GO=true ; PY=true ;;
	m) msg_count="$OPTARG" ;;
    h) echo no help for the helpless ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
: ${verbose:=false}
: ${PY:=false}
: ${C:=false}
: ${GO:=false}
$GO || $PY || C=true

: ${cmd:=${1:-run}}; test -n "$1" && shift
case "$cmd" in
run) _run "$@" ;;
esac
