//  Connects REP socket to tcp://localhost:5560

#include <math.h>
#include <time.h>
#include <unistd.h>
#include "zhelpers.h"


double square(double value, double guess)
{
    if (fabs(guess*guess - value) <= 0.001 )
		return guess;
	else
        return square(value, (guess + value/guess) / 2.0);
}

int main (int argc, char *argv[])
{
	struct timespec rem;
	struct timespec wait_time = { 0, 1 };
	void *context = zmq_ctx_new ();
	double ask;
	double result;
	char resp_msg[1001];

	//  Socket to talk to clients
	void *responder = zmq_socket (context, ZMQ_REP);
	zmq_connect (responder, "tcp://localhost:5560");

	while (1) {
		//  Wait for next request from client
		char *string = s_recv (responder);
		printf ("C worker received request: [%s]\n", string);
		ask = (double)atoi(string);
		result = square(ask, 1.0);
		free (string);

		//  Do some 'work'
		// nanosleep(&wait_time, &rem);
		snprintf(resp_msg, 1000, "workerid: %s, sqrt of %f = %f", ask, argv[1], result);

		//  Send reply back to client
		
		s_send (responder, resp_msg);
	}
	//  We never get here, but clean up anyhow
	zmq_close (responder);
	zmq_ctx_destroy (context);
	return 0;
}
