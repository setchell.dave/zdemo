package main

import (
	"database/sql"
	"fmt"
	"math"
	"strconv"
	_ "github.com/lib/pq"
	zmq "github.com/pebbe/zmq4"
	"log"
	"os" // dumb way to get args in
//	"time"
)

func square(v float64, g float64) (float64) {
	if math.Abs(g*g - v) <= 0.001 {
		return g
	} else {
		return square(v, (g + v/g) / 2)
	}
}

func main() {
	db, err := sql.Open("postgres", "postgres://postgres@localhost:5432?sslmode=disable")
	if err != nil {
		log.Fatal("Failed to open a DB connection: ", err)
	}
	defer db.Close()
	context, _ := zmq.NewContext()
	responder, _ := context.NewSocket(zmq.REP)
	responder.Connect("tcp://localhost:5560")

	rows, err := db.Query("SELECT data FROM zdemo ORDER BY id")
	if err != nil {
		log.Fatal("WOW NO ROWS: ", err)
	}
	names := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			log.Fatal(err)
		}
		names = append(names, name)
	}
	i := 0
	var result float64
	var ask float64
	for {
		//  Wait for next request from client
		request, _ := responder.Recv(0)
		fmt.Printf("Go worker received request: [%s]\n", request)
		ask, _ = strconv.ParseFloat(request, 64)
		result = square(ask, 1.0)

		// Do some 'work'
		//time.Sleep(1 * time.Nanosecond)

		// Send reply back to client
		responder.Send(fmt.Sprintf("workerid: %s, dbvalue: %s, sqrt of %f = %f", os.Args[1], names[i%len(names)], ask, result), 0)
		i++
	}
}
