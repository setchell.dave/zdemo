import zmq
import sys

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.connect("tcp://localhost:5560")

def square(value, guess):
	if abs(guess*guess - value) <= 0.001:
		return guess
	return square(value, (guess + (value/guess)) / 2 )

while True:
	message = socket.recv()
	print(f"Py worker received request: {message}")
	result=square(int(message), 1)
	socket.send_string(f'worker id: {sys.argv[1]}, sqrt of {message} = {result}')
